# Introdução 
O Banco Pan API é um serviço que retorna o saldo do cliente autorizando o token jwt no Banco Pan Login, feito para o chalenge final do MBA em engenharia de Software da FIAP.

# Tecnologias utilizadas:
- .NET Core 3.1

# Como rodar:
O SDK e as ferramentas mais recentes podem ser baixados em https://dot.net/core.

## Comandos para executar o projeto:
- dotnet restore
- dotnet build
- dotnet run --project BancoPanApi/BancoPanApi.csproj
