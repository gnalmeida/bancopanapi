﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace BancoPanApi.Controllers
{
    [ApiController]
    [Route("[controller]"), Authorize]
    public class StatementController : ControllerBase
    {
        private static readonly Dictionary<string, decimal> balance = new Dictionary<string, decimal>
        {
            { "31710429836", 1000 },
            { "10429836317", 2500 },
            { "12345678900", 1000 }
        };

        private readonly ILogger<StatementController> _logger;

        public StatementController(ILogger<StatementController> logger)
        {
            _logger = logger;
        }

        [HttpGet, Authorize(Policy = "conta_digital_Only")]
        public ActionResult Get()
        {
            var cpfCnpj = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "preferred_username").Value ?? "";

            balance.TryGetValue(cpfCnpj, out decimal saldo);

            return Ok(new { saldo });
        }

        [HttpGet("{cpfcnpj}/saldo"), Authorize(Policy = "backoffice_Only")]
        public ActionResult GetByCpfCpnj(string cpfcnpj)
        {
            balance.TryGetValue(cpfcnpj, out decimal saldo);

            return Ok(new { saldo });
        }
    }
}
